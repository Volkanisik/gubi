FROM php:7.2-apache
RUN pecl install redis-5.1.1 \
    && pecl install xdebug-2.8.1 \
    && docker-php-ext-enable redis xdebug
COPY src/ /var/www/html/
