<?php
session_start();

?>
<?php
//include "mysqlIO.php";
//$database = new mysqlIO();
$dato = date("d/m H:i");
//$madtid= $database->select('select * from gubitimestamp');

//function showTime(){
//    global $madtid;
//    $tiden = null;
//    if($madtid->num_rows>0){
//        while ($row = $madtid->fetch_assoc()){
//            $tiden = $row['tid'];
//        }
//    }
//    return $tiden;
//}

$redis = new Redis();
$redis->connect('redis',6379);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>forms</title>
    <script type="text/javascript" src="timer2/flipdown.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', () => {

            // Unix timestamp (in seconds) to count down to

            var nextdate = <?php echo $redis->get("time"); ?> ;


            // Set up FlipDown
            var flipdown = new FlipDown(nextdate, {
                theme: "light",
            }).start()
                // Start the countdown
                // Do something when the countdown ends
                .ifEnded(() => {
                    console.log('The countdown has ended!');
                });
        });
    </script>
    <link rel="stylesheet" type="text/css" href="timer2/flipdown.css">

    <link rel="stylesheet" href="default.css">
    <link rel="stylesheet" href="hover-min.css">
</head>
<body>

<h1>Dato/Tid <?php echo $dato?></h1>
<h1>Gubi skal fodres om: </h1>

<div class="flipcounter">
<div id="flipdown" class="flipdown"></div>
</div>

<br><br>
<?php
echo $_SESSION['button'];
?>
<button id="buttonmad" name="buttonmad" class="hvr-buzz" style="font-size: 30px;background-color: #18d27c;border: none;outline: none;border-radius: 5px">Mad</button>


<script>
    $("#buttonmad").click(function(){
        let password=prompt("Password:");
        $("#pass").val(password)
        $("#form").submit();
    });
</script>



<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="post" id="form">
<input id="pass" hidden name="pass" value="pass">
<!--    <button id="buttonmad" name="buttonmad" class="hvr-buzz" style="font-size: 30px;background-color: #18d27c;border: none;outline: none;border-radius: 5px">Mad</button>-->
<!--    <input type="submit" name="button" class="hvr-buzz" style="font-size: 30px;background-color: #18d27c;border: none;outline: none;border-radius: 5px" value="MAD">-->
</form>

<div id="Vejr">
    <?php
    $curl = curl_init();
    $headers = [
        'User-Agent: php'
    ];

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://vejr.eu/api.php?location=Stenlose&degree=C",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => $headers,
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $vejrdata = json_decode($response);
    $temperatur=array($vejrdata->CurrentData->temperature,$vejrdata->CurrentData->skyText);
    echo "<h2>Vejr: ".$temperatur[1]." ".$temperatur[0]." &#8451;</h2>";

    if(isset($_POST['pass']) && $_SESSION['button']!=1){
        $pass = $redis->get("password");
        if($_POST['pass'] == $pass) {
            $redis->set("time", (time() + (3 * 24 * 60 * 60)));
        }
        $_SESSION['button'] = 1;
        echo "<script> window.location.reload(); </script>";
    }
    ?>

</div>
</body>
</html>
