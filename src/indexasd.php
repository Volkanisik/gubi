<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>forms</title>
    <link rel="stylesheet" href="default.css">
    <link rel="stylesheet" href="hover-min.css">
</head>
<body>
<?php
$dato = date("d/m H:i");
$filen=fopen("datoen.txt","r");
$madtid=fgets($filen);
fclose($filen);
$interval = null;

if(($_SERVER["REQUEST_METHOD"]=="POST") && ($_SESSION['button']!=1)){
   $file = fopen("datoen.txt","w");
   $nydato = time();
   $nydato = $nydato+3600*24*3;
   fwrite($file,$nydato);
   fclose($file);
   $_SESSION['button'] = 1;
   header("Refresh:2");

}
function showTime(){
    global $interval;
    global $madtid;
    $interval = time();
    $interval = $madtid - $interval;
    $tidentilbage = ceil($interval/3600)." timer ".ceil(($interval%3600)/60)." minutter og ".ceil($interval%60)." sekunder";
    return $tidentilbage;
}

?>
<h1>Dato/Tid <?php echo $dato?></h1>
<h1>Gubi skal fodres om: <?php echo showTime()?> </h1>
<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="post">
    <input type="submit" class="hvr-buzz" style="font-size: 30px;background-color: #18d27c;border: none;outline: none;border-radius: 5px" value="MAD">
</form>

<div id="Vejr">
    <?php
    $curl = curl_init();
    $headers = [
        'User-Agent: php'
    ];

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://vejr.eu/api.php?location=Stenlose&degree=C",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => $headers,
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $vejrdata = json_decode($response);
    $temperatur=array($vejrdata->CurrentData->temperature,$vejrdata->CurrentData->skyText);
    echo "<h2>Vejr: ".$temperatur[1]." ".$temperatur[0]." &#8451;</h2>"
    ?>

</div>
</body>
</html>

<?php
