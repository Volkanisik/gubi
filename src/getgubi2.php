<?php
include "mysqlIO.php";
header("Content-Type: application/json");
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $database = new mysqlIO();
    $dato = date("d/m H:i");
    $madtid = $database->select('select * from gubimad');

    function showTime()
    {
        global $madtid;
        $tiden = null;
        if ($madtid->num_rows > 0) {
            while ($row = $madtid->fetch_assoc()) {
                $tiden = $row['tid'];
            }
        }
        return $tiden;
    }

    echo showTime();
}